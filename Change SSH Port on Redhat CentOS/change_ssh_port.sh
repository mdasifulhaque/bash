#!/bin/bash

default_port=22/tcp
ssh_file_path=/etc/ssh/sshd_config


function check_port()
{
    if [ -z "$1" ];
    then 
    read -p "Enter Valid Port Number: " port
    temp_port=$port"/tcp"
    check_port "$temp"
    else 
    temp_port=$1"/tcp"
    is_valid=$(firewall-cmd --query-port=$temp_port)
        if [[ $is_valid == yes ]];
        then 
            echo "Port $temp_port already defined please enter different port"
            read -p "Enter Valid Port Number: " port
            temp_port=$port"/tcp"
            new_port=$port
            check_port "$port"
        else
        valid_port_number="no"
        fi
    fi
}


function change_port()
{
    echo "Value is $1"
    date_format=$(date +%Y_%m_%d_%H_%M_%S)
    backup_file=$ssh_file_path
    backup_file+="_"
    backup_file+=$date_format
    cp $ssh_file_path ${backup_file}
    echo "Changing to new port number $1 ...."
    sed -i 's%#Port 22%Port '$1'%g' $ssh_file_path
    echo "New Information $1"
    grep -i "Port $1" $ssh_file_path
    echo "Updating SELinux Information"
    semanage port -a -t ssh_port_t -p tcp $1
    echo "Updated information "
    semanage port -l | grep ssh
    echo "Adding Newly added port $1 to firewall "
    new_port=$1/tcp
    firewall-cmd --add-port=$new_port --permanent
    echo "Reloading firewall information"
    firewall-cmd --reload
    echo "Removing SSH server "
    firewall-cmd --remove-service=ssh --permanent
    echo "'"
    firewall-cmd --reload
    echo "Restarting sshd Service"
    systemctl restart sshd 
    echo "SSHD service Status...."
    systemctl status sshd
    echo ""
    netstat -tunl | grep $1
    echo ""
    echo "Press ENTER to exit"
    read junk
}


function check_root_user()
{
    
    if [ $EUID -ne 0 ];
    then 
    root_user='NO'
    else
    root_user='YES'
    fi
}

check_root_user 
if [[ $root_user == YES ]];
   then 
    read -p "Enter New Port Number: " new_port
    check_port "$new_port"
        if [[ $valid_port_number == no ]];
            then 
             change_port "$port"
        fi
else
    echo "This script require sudo access. Please run with sudo script_name.sh"
    exit 1
fi

